﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using Microsoft.Web.WebSockets;
namespace tp
{
    /// <summary>
    /// Summary description for websocket
    /// </summary>
    public class websocket : IHttpHandler
    {
       
        public void ProcessRequest(HttpContext context)
        {
            if (context.IsWebSocketRequest)
            {
                Debug.WriteLine("ManagedThreadId = " + Thread.CurrentThread.ManagedThreadId);
                context.AcceptWebSocketRequest(new socket());
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}