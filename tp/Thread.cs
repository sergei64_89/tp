﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using tp.ServiceReference1;
using System.ServiceModel;
using Microsoft.Web.WebSockets;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Diagnostics;
using tp.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using tp.special;
using System.Collections.ObjectModel;
namespace tp
{
    [CallbackBehavior(UseSynchronizationContext = false)]
    public class callback : ServiceReference1.IPublisherServiceCallback
    {
        private string serialize(object b)
        {
            zaivka_ajax zz = (zaivka_ajax)b;
         

            JavaScriptSerializer can = new JavaScriptSerializer();
            
            return can.Serialize(zz);
           

        }
        public  IHubContext icontext = GlobalHost.ConnectionManager.GetHubContext<events>();
        private  List<socket> list;
        private PublisherServiceClient wcfClient;
        public callback(List<socket> list)
        {
            this.list = list;
            wcfClient = Auth.get_client();
        
        }
        public void Notify()
        {

        }
        public void Change_status_zaivki(zaivka z)
        {
            zaivka_ajax help = new zaivka_ajax(z, "status");
           // send(help, list);
            sendtest(help,events.AllClients);
        }
        //событие изменения собственника заявки
        public void Changed_user_zaivki(zaivka z)
        {
            zaivka_ajax help = new zaivka_ajax(z, "changeduser");
            // send(help, list.Where(n => n.name == z.new_login).ToList());
            sendtest(help,events.AllClients);
        }
        //оповещение для клиента кому передаетс заявка
        public void Changing_user_zaivki(zaivka z)
        {
            zaivka_ajax help = new zaivka_ajax(z, "user");
           // send(help, list.Where(n => n.name == z.new_login).ToList());
            sendtest(help,events.AllClients.Where(n => z.new_logins.Exists(f=>f== n.login)).ToList());
        }

        public void Event_to_User(string login, Notification_to_user notic)
        {
            List<client> list = events.AllClients.Where(n => login == n.login).ToList();
 
            sendNotification(notic, list);
        }

        public void Event_to_Asp(List<string> logins, Notification_to_user notic)
        {
          List<client> list=  events.AllClients.Where(n => logins.Exists(m => m == n.login)).ToList();
         
          sendNotification(notic, list);
        }
        #region отсылка клиенту
        private void send(zaivka_ajax z,List<socket> list)
        {
           
            foreach (socket s in list)
            {
                JavaScriptSerializer ser = new JavaScriptSerializer();
                s.Send(ser.Serialize(z));
            }
        }
        private void sendtest(zaivka_ajax z,List<client> allclient)
        {
            bool h;
            foreach (client c in allclient.ToArray())
            {
                if (wcfClient.State != CommunicationState.Opened)
                {
                    wcfClient = Auth.get_client();
                    if (wcfClient.State != CommunicationState.Opened)
                        return;
                }
                wcfClient.can_user_know_about_zaivka(new user { login = c.login }, z.z.id, out h);
                if ((h == true) ||(z.type=="remove"))
                {
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    try
                    {
                        icontext.Clients.Client(c.connectionId).Send(ser.Serialize(z));
                    }
                    catch { }
                }
                
            }

        }
        private void sendNotification(Notification_to_user z, List<client> allclient)
        {
            foreach (client c in allclient.ToArray())
            {
                JavaScriptSerializer ser = new JavaScriptSerializer();
                try
                {
                    icontext.Clients.Client(c.connectionId).NotificationUser(ser.Serialize(z));
                }
                catch
                {

                }
            }
        }
        #endregion
        //событие изменения состояния заявки
        public void Change_sostoinie_zaivki(zaivka z)
        {
              zaivka_ajax help ;
              if (z.state_name != "end")
              
                  help = new zaivka_ajax(z, "old");
                
              
              else
              
                  help = new zaivka_ajax(z, "remove");
                 // send(help, list);
              sendtest(help,events.AllClients);
        


        }
        //событие создание новой заявки
        public void new_zaivka(zaivka z)
        {
            zaivka_ajax help = new zaivka_ajax(z, "new");
           // send(help, list);
            sendtest(help,events.AllClients);
        }
    }
    public class Potok
    {
        
        public Potok(List<socket> list)
        {
            this.list = list;
            main();
          
        }
        List<socket> list;
       public static ServiceReference1.PublisherServiceClient client;
        InstanceContext back;
        private void rerun(Object sender,
    EventArgs e)
        {
            while (true)
            {
               
               
                    try
                    {
                      
                        //client.Abort();
                        client = new PublisherServiceClient(back);
                        client.Open();
                        client.add_change_sostoinie_zaivki(new user { login = "all", password = "32167" });
                 
                        client.InnerDuplexChannel.Faulted += new EventHandler(rerun);
                        client.InnerDuplexChannel.Closed += new EventHandler(rerun);
                        break;
                    }
                    catch
                    {
                    }



                

            }
            
        }
        private void main()
        {
           
           back = new InstanceContext(new callback(list));
            client = new PublisherServiceClient(back);
            client.Open();
            client.add_change_sostoinie_zaivki(new user { login = "all", password = "32167" });
            
            client.InnerDuplexChannel.Faulted += new EventHandler(rerun);
            client.InnerDuplexChannel.Closed += new EventHandler(rerun);
         

        }
    }
}