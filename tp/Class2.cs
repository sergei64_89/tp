﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Providers.Entities;
using System.Web.Routing;
using System.Web.Security;
using tp.special;

namespace tp
{
    public class IsGuestAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if ((filterContext.HttpContext.Request.Cookies["user"] == null) || (filterContext.HttpContext.Request.Cookies["pass"] == null))
            {
                help(filterContext);
            }
            else
            {
                ServiceReference1.PublisherServiceClient client = Auth.get_client();
                if (!client.login(Auth.get_user()))
                {
                    help(filterContext);
                }
                 
            }



        }
        private void help(ActionExecutingContext filterContext)
        {

            filterContext.HttpContext.Response.Cookies.Clear();
            //FormsAuthentication.SignOut();
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
            {
                action = "Index",
                controller = "Auth",

            }));
        }
    }
}