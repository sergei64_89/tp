﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tp.Models;
using tp.special;

namespace tp.Controllers
{
    public class HistoryZaivokController : Controller
    {
        //
        // GET: /HistoryZaivok/

        public ActionResult Index(ServiceReference1.filter ff,int PageNum=0)
        {
             List<string> schems, users;
            Int64 count;
             ServiceReference1.PublisherServiceClient client = Auth.get_client();
             List<ServiceReference1.zaivka_history> list;
            
            client.get_schems(Auth.get_user(), out schems);
            client.get_users_list(Auth.get_user(), out users);
            client.getzaivkihistory(Auth.get_user(),ff, PageNum * 15, (PageNum + 1) * 15, out count, out list);
            findObject hh = new findObject { param = ff,schems=schems,users=users };
       
            ViewData["findobject"] = hh;

            vspomogatelnii.get(count, PageNum, 15, this.ViewData);


            return View(list);
        }
        public ActionResult find(FormCollection collection)
        {
            List<ServiceReference1.zaivka_history> list;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
        ServiceReference1.filter ff=    HomeController.filterparam(collection);
          

          return  RedirectToAction("Index",ff);
           
        }
        public ActionResult historysostoinii(Int64 id)
        {
            ServiceReference1.zaivka_history z_h;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.getzaivkahistory(Auth.get_user(),id,out z_h);
            ViewData["getstatuses"] = Url.Action("gethistorystatuses");
            ViewData["gethistorysended"] = Url.Action("gethistorysended");
            return View("DetailHistoryZaivki", z_h);
        }
        #region актуальная заявка
        public ActionResult getzaivkaactualhistory(Int64 id)
        {
            ServiceReference1.zaivka_history z_h;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.get_history_actualzaivki(Auth.get_user(), id, out z_h);
            ViewData["getstatuses"] = Url.Action("gethistoryactualstatuses");
            ViewData["gethistorysended"] = Url.Action("gethistoryactualsended");
            return View("DetailHistoryZaivki", z_h);
        }
        public JsonResult gethistoryactualsended(string id, string z_id)
        {
            ServiceReference1.zaivka_history z_h;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.get_history_actualzaivki(Auth.get_user(), Convert.ToInt64(z_id), out z_h);
            return Json(z_h.list.Where(m => m.ID == Convert.ToInt64(id)));
        }
        public JsonResult gethistoryactualstatuses(string id, string z_id)
        {
            ServiceReference1.zaivka_history z_h;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.get_history_actualzaivki(Auth.get_user(), Convert.ToInt64(z_id), out z_h);
            return Json(z_h.list.Where(m => m.ID == Convert.ToInt64(id)));
        }
        #endregion


        public JsonResult gethistorysended(string id, string z_id)
        {
            ServiceReference1.zaivka_history z_h;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.getzaivkahistory(Auth.get_user(), Convert.ToInt64(z_id), out z_h);
            return Json(z_h.list.Where(m => m.ID == Convert.ToInt64(id)));
        }
        public JsonResult gethistorystatuses(string id,string z_id)
        {
            ServiceReference1.zaivka_history z_h;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.getzaivkahistory(Auth.get_user(),Convert.ToInt64(z_id), out z_h);
            return Json(z_h.list.Where(m => m.ID ==Convert.ToInt64( id)));
        }
    }
}
