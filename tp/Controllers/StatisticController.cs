﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tp.special;
using tp.ServiceReference1;
using tp.Models;
namespace tp.Controllers
{
    public class StatisticController : Controller
    {
        //
        // GET: /Statistic/

        
        public ActionResult Index(string typestatistic="StatisticSotoinie")
        {

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            if (!client.isboss(Auth.get_user().login))
            {
                ViewData["error_text"] = "нет прав!!!!";

                return View("userError");
            }
            switch (typestatistic)
            {
                case "StatisticSotoinie":

                   



                    ViewData["list"] = TempData["StatisticSotoinie"] as List<dicttime>;
                    List<shemsost> listsosoinii=   client.getschemssostoinie();
                List<string> users, schems;
                client.get_schems(Auth.get_user(), out schems);
                client.get_users_list(Auth.get_user(), out users);
            
                filter ff = (filter)TempData["filterparam"];
                ff = ff == null ? new filter() : ff;
                findObject obj = new findObject { param = ff, users = users, schems = schems, shem_sostoinii = listsosoinii };
                ViewData["filter"] = obj;
                client.Close();
                return View("StatisticSostoinie",obj);
                    break;
                default :
                    return View();
                    break;
            }
           
        }

        


        public ActionResult StatisticSotoinie(FormCollection collection)
        {
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            string schema = collection["schem_value"];
            string user = collection["user_value"];
            string sostoinie_value = collection["sost_value"];

           

            List<dicttime> list;
         error err=client.GetStatistics_sostoinie(Auth.get_user(),user, sostoinie_value, schema, out list);
         TempData["statistic"] = list;
         if (err.Is_error == true)
         {

             ViewData["error_text"] = err.error_text;
                 return View("userError");
         }
         else
         {
             TempData["StatisticSotoinie"] = list;
             return RedirectToAction("Index", new { typestatistic = "StatisticSotoinie" });
         }

        }

    }
}
