﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using tp.ServiceReference1;
using tp.special;
namespace tp.Controllers
{
    public class AuthController : Controller
    {
        //
        // GET: /Auth/
      
        public ActionResult Index()
        {
           
         //   if (Request.IsAuthenticated)
         //      return RedirectToAction("Index", "Home");
            return View();
        }
        [HttpPost]
        public ActionResult Login(user current)
        {
            if (Auth.login(current) == false)
                return RedirectToAction("Index");
            else
            {
              HttpCookie cookie = new HttpCookie("pass");
cookie.Value = current.password;
cookie.Expires = DateTime.Now.AddHours(12);

this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
cookie = new HttpCookie("user");
cookie.Value = current.login;
cookie.Expires = DateTime.Now.AddHours(12);
this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
                
               // FormsAuthentication.SetAuthCookie(current.login,false);
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult Logoff()
        {
            //FormsAuthentication.SignOut();

            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }
            return RedirectToAction("Index", "Auth");
        }
    }
}
