﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tp.special;

namespace tp.Controllers
{
    public class New_zaivkaController : Controller
    {
        //
        // GET: /New_zaivka/
              [IsGuest]
        public ActionResult Index()
        {
            List<string> list;
           ServiceReference1.PublisherServiceClient client=Auth.get_client();
           client.get_schems_create(Auth.get_user(), out list);
           client.Close();
            return View(list);
        }
              [IsGuest]
        public ActionResult create(FormCollection collection)
        {
           ServiceReference1.zaivka z;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            if (collection["schema"] != null)
            {
                ServiceReference1.error err = client.add_new_zaivka(collection["schema"], Auth.get_user(), out z);
                client.Close();
                if (err.Is_error == true)
                {
                    ViewData["error_text"] = err.error_text;
                    
                    return View("userError");
                }
               
                return RedirectToAction("detail", "Detail", new { number = z.id });
            }
            else
                return RedirectToAction("Index", "New_zaivka");
          
            //return View("", z);
        }
    }
}
