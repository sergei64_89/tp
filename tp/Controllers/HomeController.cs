﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using tp.special;
using tp.ServiceReference1;
using tp.Models;
using System.Diagnostics;
using System.Security.Principal;
using System.Threading;
namespace tp.Controllers
{
   
    public class HomeController : Controller
    {
        int max = 15;

        public JsonResult getuser(string login)
        {
            user hh;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.getfulluser(Auth.get_user(), login, out hh);
            return Json(hh);
        }
        //
        // GET: /Home/
        /*
        //вкладка технические процессы
       [Authorize, IsGuestAttribute]
        public ActionResult Index(filter ff,int PageNum=0)
        {
      
            ViewData["type"] = "Index";
           List<zaivka> list;
           ServiceReference1.PublisherServiceClient client = Auth.get_client();
           Stopwatch h = Stopwatch.StartNew();
           error er = client.get_zaivki(Auth.get_user(), out list);
           h.Stop();
           ViewData["error_text"]=er.error_text;
           if (er.Is_error == true)
               return View("userError");
           list = filter(list, ff);
           list = get(list, PageNum);
         string hh=  Request.QueryString.ToString();
          

        
           List<string> users,schems;
           client.get_schems(Auth.get_user(),out schems);
           client.get_users_list(Auth.get_user(),out users);
           List<string> statuses;
           client.get_statuses(Auth.get_user(), out statuses);
           findObject obj = new findObject { param = ff,states=statuses, users = users, schems = schems };
           ViewData["filter"] = obj;
           client.Close();
            return View(list);
          
        }
         * [IsGuest]
        public ActionResult Mytp(filter ff,int PageNum=0)
        {

            ViewData["type"] = "Mytp";
           List<zaivka> list;
           ServiceReference1.PublisherServiceClient client = Auth.get_client();
       error er=    client.get_my_zaivki(Auth.get_user(), out list);
     
       if (er.Is_error == true)
       {
           ViewData["error_text"] = er.error_text;
           return View("userError");
       }
           list = filter(list, ff);
           list = get(list, PageNum);
         string hh=  Request.QueryString.ToString();
         List<string> users, schems;
         client.get_schems(Auth.get_user(), out schems);
         client.get_users_list(Auth.get_user(), out users);
         List<string> statuses;
         client.get_statuses(Auth.get_user(), out statuses);
         findObject obj = new findObject { param = ff,states=statuses, users = users, schems = schems };
         ViewData["filter"] = obj;

         client.Close();

                 
            return View("Index",list);
          
        }
         * //переход н вклвдку конторля
       public ActionResult control(filter ff, int PageNum = 0)
        {
            List<zaivkaControl> list;
            PublisherServiceClient client = Auth.get_client();
            if (client.isboss(Auth.get_user().login))
            {
                client.getzaivkiofcontrol(Auth.get_user(), out list);
                list = filter2(list, ff);
                list = get2(list, PageNum);

                List<string> users, schems;
                client.get_schems(Auth.get_user(), out schems);
                client.get_users_list(Auth.get_user(), out users);
                List<string> statuses;
                client.get_statuses(Auth.get_user(), out statuses);
                findObject obj = new findObject { param = ff,states=statuses, users = users, schems = schems };
                ViewData["filter"] = obj;
                client.Close();
                return View(list);
            }
            client.Close();
            return RedirectToAction("Index","Home");
        }
         * */
        //вкладка технические процессы
      [IsGuest]
       public ActionResult Index(filter ff, int PageNum = 0)
       {
           ViewData["type"] = "Index";
           List<zaivka> list;
           ServiceReference1.PublisherServiceClient client = Auth.get_client();
          
           Int64 count;
           string windowsName = WindowsPrincipal.Current.Identity.Name;
           //OR
            windowsName = Thread.CurrentPrincipal.Identity.Name;
           error er = client.get_zaivki_offset(Auth.get_user(),ff,PageNum*15,(PageNum+1)*15,out count,  out list);
          
           ViewData["error_text"] = er.error_text;
           if (er.Is_error == true)
               return View("userError");
      
            get(count, PageNum);



            List<shemsost> listsosoinii = client.getschemssostoinie();
           List<string> users, schems;
           client.get_schems(Auth.get_user(), out schems);
           client.get_users_list(Auth.get_user(), out users);
           List<string> statuses;
           client.get_statuses(Auth.get_user(), out statuses);
           findObject obj = new findObject { param = ff, states = statuses, users = users, schems = schems, shem_sostoinii = listsosoinii, kategorii = new List<string> { "Обычная","Важные"} };
           ViewData["filter"] = obj;
           client.Close();
           return View(list);
       }
        //переход н вклвдку конторля
       public ActionResult control(filter ff, int PageNum = 0)
        {
            List<zaivkaControl> list;
            Int64 count;
            PublisherServiceClient client = Auth.get_client();
            if (client.isboss(Auth.get_user().login))
            {
                client.getzaivkiofcontroloffset(Auth.get_user(), ff, PageNum * 15, (PageNum + 1) * 15, out count, out list);
                get(count, PageNum);


             List<shemsost> listsosoinii=   client.getschemssostoinie();
                List<string> users, schems;
                client.get_schems(Auth.get_user(), out schems);
                client.get_users_list(Auth.get_user(), out users);
                List<string> statuses;
                client.get_statuses(Auth.get_user(), out statuses);
                findObject obj = new findObject { param = ff, states = statuses, users = users, schems = schems, shem_sostoinii = listsosoinii, kategorii = new List<string> { "Обычная", "Важные" } };
                ViewData["filter"] = obj;
                client.Close();
                return View(list);
            }
            client.Close();
            return RedirectToAction("Index","Home");
        }
       public void iscontrol(Int64 id, bool ischecked)
        {
            PublisherServiceClient client=Auth.get_client();
      
            client.setcontrol(Auth.get_user(),id,ischecked);
            client.Close();
          
            
        }
              [IsGuest]
        public ActionResult Mytp(filter ff,int PageNum=0)
        {
            ff.user_value = Auth.get_user().login;
            ViewData["type"] = "Mytp";
            List<zaivka> list;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            Stopwatch h = Stopwatch.StartNew();
            Int64 count;

            error er = client.get_zaivki_offset(Auth.get_user(), ff, PageNum * 15, (PageNum + 1) * 15, out count, out list);
            h.Stop();
            ViewData["error_text"] = er.error_text;
            if (er.Is_error == true)
                return View("userError");

            get(count, PageNum);



            List<shemsost> listsosoinii = client.getschemssostoinie();
            List<string> users, schems;
            client.get_schems(Auth.get_user(), out schems);
            client.get_users_list(Auth.get_user(), out users);
            List<string> statuses;
            client.get_statuses(Auth.get_user(), out statuses);
            findObject obj = new findObject { param = ff, states = statuses, users = users, schems = schems,shem_sostoinii=listsosoinii, kategorii = new List<string> { "Обычная","Важные"} };
            ViewData["filter"] = obj;
            client.Close();
            
            return View("Index",list);
          
        }

              #region
              private void get(Int64 Count, int PageNum)
              {
                  Int64 count;
                  count = Count / max;
                  if (Count % max > 0)
                      count++;

                  if ((PageNum < 0) || (count < PageNum + 1))
                      PageNum = 0;
                  if (Count < max)
                  {
                      ViewData["next"] = -1;
                      ViewData["preview"] = -1;
                  }
                  else
                  {
                      ViewData["next"] = PageNum + 1;
                      ViewData["preview"] = PageNum - 1;
                  }

                  if (PageNum == 0)
                  {
                      ViewData["preview"] = -1;
                  }

                  if ((count == PageNum + 1))
                  {
                      ViewData["next"] = -1;

                  }



                  ViewData["PageNum"] = PageNum;
                 








                  
              }
              private List<zaivka> get(List<zaivka> list,int PageNum)
        {
            int count;
            count=list.Count / max;
            if (list.Count % max>0)
                count++;

            if ((PageNum < 0) || (count < PageNum+1))
                PageNum = 0;
            if (list.Count < max)
            {
                ViewData["next"] = -1;
                ViewData["preview"] = -1;
            }
            else
            {
                ViewData["next"] = PageNum + 1;
                ViewData["preview"] = PageNum - 1;
            }

            if (PageNum == 0)
            {
                ViewData["preview"] = -1;
            }

            if ((count== PageNum+1))
            {
                ViewData["next"] = -1;

            }
            

         
                ViewData["PageNum"] = PageNum;
                list = list.Skip(max * PageNum).Take(max).ToList();

               
       
             
          

           
            
            return list;
        }
              private List<zaivka> filter(List<zaivka> list,filter filt)
              {
                  
                  if (filt.state_value != null)
                  {
                      list = list.Where(n => n.current_status == filt.state_value).ToList();
                    
                  }
                  if (filt.schem_value != null)
                  {
                      list = list.Where(n => n.schema == filt.schem_value).ToList();
                
                  }
                  if (filt.sost_value != null)
                      list = list.Where(n => n.state_name == filt.sost_value).ToList();
                  if (filt.user_value != null)
                      list = list.Where(n => n.current_login == filt.user_value).ToList();
              
                  return list;
              }

              private List<zaivkaControl> get2(List<zaivkaControl> list, int PageNum)
              {
                  int count;
                  count = list.Count / max;
                  if (list.Count % max > 0)
                      count++;

                  if ((PageNum < 0) || (count < PageNum + 1))
                      PageNum = 0;
                  if (list.Count < max)
                  {
                      ViewData["next"] = -1;
                      ViewData["preview"] = -1;
                  }
                  else
                  {
                      ViewData["next"] = PageNum + 1;
                      ViewData["preview"] = PageNum - 1;
                  }

                  if (PageNum == 0)
                  {
                      ViewData["preview"] = -1;
                  }

                  if ((count == PageNum + 1))
                  {
                      ViewData["next"] = -1;

                  }



                  ViewData["PageNum"] = PageNum;
                  list = list.Skip(max * PageNum).Take(max).ToList();








                  return list;
              }
              private List<zaivkaControl> filter2(List<zaivkaControl> list, filter filt)
              {

                  if (filt.state_value != null)
                  {
                      list = list.Where(n => n.state_name == filt.state_value).ToList();

                  }
                  if (filt.schem_value != null)
                  {
                      list = list.Where(n => n.schema == filt.schem_value).ToList();

                  }
                  if (filt.sost_value != null)
                      list = list.Where(n => n.state_name == filt.sost_value).ToList();
                  if (filt.user_value != null)
                      list = list.Where(n => n.current_login == filt.user_value).ToList();
                  if (filt.iscontrol == true)
                      list = list.Where(n => n.iscontrol == true).ToList();
                  return list;
              }



              public static filter filterparam(FormCollection collection)
              {

                  filter ret = new filter { state_value = "", schem_value = "",sost_value="",user_value="" };
                  if (collection["state"] != null)
                  {
                     
                      ret.state_value = collection["state_value"];
                  }
                  if (collection["schem"] != null)
                  {
                     
                      ret.schem_value = collection["schem_value"];
                  }
                  if (collection["sost"] != null)
                  {

                      ret.sost_value = collection["sost_value"];
                  }
                  if (collection["kategorii"] != null)
                  {

                      ret.kategorii_value = collection["kategorii_value"];
                  }
                  if (collection["iscontrol"] != null)
                  {
                      ret.iscontrol = true;
                  }
                  
                  else
                      ret.iscontrol = false;
                      if (collection["users"]!=null)
                  {
                      ret.user_value = collection["user_value"];
                  }


                      if (collection["usersend"] != null)
                      {
                          ret.user_end = collection["userend_value"];
                      }

                      if (collection["isdate"] != null)
                      {
                          try
                          {
                              ret.begin = Convert.ToDateTime(collection["begin"]);
                              ret.end = Convert.ToDateTime(collection["end"]);
                          }
                          catch
                          {
                          }
                      }
                  return ret;
                 
              }

              #endregion
              [IsGuest] 
        public ActionResult Update(string type,string asc_desc)
        {
            List<zaivka> list;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.get_my_zaivki(Auth.get_user(), out list);
          
            switch (type)
            {
                case "состояние":
                    if (asc_desc == "asc")
                        list = list.OrderBy(n => n.current_state).ToList();
                    else
                        list = list.OrderByDescending(n => n.current_state).ToList();
                    break;
                case "схема":
                    if (asc_desc=="asc")
                  list=  list.OrderBy(n => n.schema).ToList();
                    else
                        list = list.OrderByDescending(n => n.schema).ToList();
                    break;
                case "пользователь":
                      if (asc_desc=="asc")
                  list=  list.OrderBy(n => n.current_login).ToList();
                    else
                        list = list.OrderByDescending(n => n.current_login).ToList();
                    break;
            }
            return Json(list,JsonRequestBehavior.AllowGet);
        }
              [IsGuest]
        public JsonResult get_sending_tp(string type, string asc_desc)
        {
            List<zaivka> list;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.get_sending_zaivki(Auth.get_user(), out list);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
              [IsGuest]
              public ActionResult take(Int64 number)
              {
                  ServiceReference1.PublisherServiceClient client = Auth.get_client();
                  client.take_sending_zaivki(number, Auth.get_user());
                  client.Close();
                  return RedirectToAction("detail","Detail", new { number = number });

              }
              public ActionResult filter(FormCollection collection)
              {
                  Request.QueryString.ToString();
                 
                  string hh=collection["action"];
                  return RedirectToAction(hh, "Home", filterparam(collection));
              }
              public JsonResult isvalidation()
              {
                  ServiceReference1.PublisherServiceClient client = Auth.get_client();
                return  Json(client.login(Auth.get_user()),JsonRequestBehavior.AllowGet);
              }
    }
}
