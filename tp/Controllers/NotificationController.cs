﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tp.ServiceReference1;
using tp.special;

namespace tp.Controllers
{
    public class NotificationController : Controller
    {
        //
        // GET: /Notification/

        public ActionResult Index()
        {
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            List<Notification_to_user> listtt;
            client.get_Notifications_user(Auth.get_user(), out listtt);
            TempData["notifications"] = listtt;
            return View();
        }

    }
}
