﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tp.special;
using tp.ServiceReference1;

namespace tp.Controllers
{
    public partial class DetailController : Controller
    {
        //
        // GET: /Detail/
       
              [IsGuest]
        public ActionResult detail(Int64 number)
        {
         HttpContext.Response.AppendCookie(new HttpCookie("id_zaivki",number.ToString()));
      
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka z;
          client.get_full_zaivka_for_id(Auth.get_user(),number,out z);
           if (z == null)
               return RedirectToAction("Index", "Home");
            List<string> help_list ;
            client.get_users_for_zaivka(Auth.get_user(),number, out help_list);
           
           List<string> statuses;
                  List<string> help_list2;
           client.get_statuses(Auth.get_user(), out statuses);
           client.get_roles_for_zaivka(Auth.get_user(), number, out help_list2);
           ViewData["role_list"] = help_list2;
           ViewData["user_list"] = help_list;
            ViewData["statuses"] = statuses;
            bool is_owner,is_can;
                  //Принять Заявку!?
            if (z.new_logins.Exists(n=>n == Auth.get_user().login))
                ViewData["is_take"] = true;
            else
                ViewData["is_take"] = false;
            
                  //отмена передачи другому пользователю!?
            if ((z.current_login == Auth.get_user().login) && (z.new_logins.Count != 0))
                ViewData["is_back"] = true;
            else
                ViewData["is_back"] = false;
                  client.can_user_do_operation(Auth.get_user(),  "Передача ТП",z.id,out is_owner,out is_can);
                  //доступна ли операция перехода на следующее состояние
            if (is_can)
            {
                ViewData["CanSendToOther"] = true;
            }
            else
            {
                
                ViewData["CanSendToOther"] = false;
            }

            client.can_user_do_operation(Auth.get_user(), "Изменение статуса", z.id, out is_owner, out is_can);
            if (is_can==true)
            {
                ViewData["CanChangeStatus"] = true;
            }
            else
            {

                ViewData["CanChangeStatus"] = false;
            }
           
            object ob;
            userPartial.get_parameters(z, "detail", ViewData,TempData, out ob);
            client.Close();
                return View(z);
        }
             [IsGuest]
        public ActionResult send(string id_zaivki)
              {
          
                  Int64 number = Convert.ToInt64(id_zaivki);
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.go_next_state(number, Auth.get_user());
            client.Close();
            return RedirectToAction("detail", new { number=number});
        }
             [IsGuest]
        public ActionResult Change(FormCollection collection)
        {
                Int64 f =Convert.ToInt64(collection["id_zaivki"]);
            if (collection["Status"] != null)
            {
            
                ServiceReference1.PublisherServiceClient client = Auth.get_client();
                client.Change_status(Auth.get_user(),collection["status_value"],f);
                client.Close();
          
            }
            else
            
                if ((collection["send"] != null) && (collection["user_send"] != null))
                {
                    ServiceReference1.PublisherServiceClient client = Auth.get_client();
                    client.send_zaivka_to_user(f, collection["user_send"],null, Auth.get_user());
                    client.Close();
                  
                }
                    else
                    if (collection["take_back"] != null)
                    {
                        ServiceReference1.PublisherServiceClient client = Auth.get_client();
                        client.take_back_sending_zaivki(Auth.get_user(), f);
                        client.Close();
                       
                    }
                    else
                        if (collection["take_zaivka"] != null)
                    {
                        ServiceReference1.PublisherServiceClient client = Auth.get_client();
                        client.take_sending_zaivki(f, Auth.get_user());
                        client.Close();
                      


                    }
                        else

                            if ((collection["send_to_role"] != null)  && (collection["user_send_role"] != null))
                            {
                                ServiceReference1.PublisherServiceClient client = Auth.get_client();
                                client.send_zaivka_to_user(f, null, collection["user_send_role"], Auth.get_user());
                                client.Close();
                            }
                return RedirectToAction("detail", new { number = f });    

            
                
        }
        
    }
}
