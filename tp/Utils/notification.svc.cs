﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using tp.special;

namespace tp
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class notification
    {
        // Чтобы использовать протокол HTTP GET, добавьте атрибут [WebGet]. (По умолчанию ResponseFormat имеет значение WebMessageFormat.Json.)
        // Чтобы создать операцию, возвращающую XML,
        //     добавьте [WebGet(ResponseFormat=WebMessageFormat.Xml)]
        //     и включите следующую строку в текст операции:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        [WebInvoke (Method="POST",BodyStyle=WebMessageBodyStyle.Wrapped,ResponseFormat=WebMessageFormat.Json,UriTemplate="gettypes/{id}")]
        public List<string> GetNotificationzaivki(string  id)
        {
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            List<string> list;
            client.get_types_notification_zaivki_for_user(Auth.get_user(),Convert.ToInt64(id), out list);
            return list;

        }
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat=WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "settypes/{id}")]
        public bool setNotifications_to_zaivka(List<string> list,string id)
        {
              
            ServiceReference1.PublisherServiceClient client = Auth.get_client();

            if (client.set_types_control_zaivki_to_user(Auth.get_user(),Convert.ToInt64(id), list).Is_error == false)
                return true;
            else return false;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "removenotification/{id}")]
        public bool removenotification(string id)
        {
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            if (client.removeNotification(Auth.get_user(), Convert.ToInt64(id)).Is_error == false)
                return true;
            else
                return false;
        }
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "setNotificationused/{id}")]
        public bool setNotificationused(string id)
        {
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            if (client.setNotificationused(Auth.get_user(), Convert.ToInt64(id)).Is_error == false)
                return true;
            else
                return false;
        }
        // Добавьте здесь дополнительные операции и отметьте их атрибутом [OperationContract]
    }
}
