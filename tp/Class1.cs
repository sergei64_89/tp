﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
namespace tp
{
    public class MyViewEngine : RazorViewEngine
    {

        private static string[] NewPartialViewFormats = new[] {
       
        "~/develop/{0}.cshtml"
    };

        public MyViewEngine()
        {
           
            base.PartialViewLocationFormats = base.PartialViewLocationFormats.Union(NewPartialViewFormats).ToArray();
        }

    }
    /*
    public class TweetModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var contentType = controllerContext.HttpContext.Request.ContentType;
       

            string bodyText;

            using (var stream = controllerContext.HttpContext.Request.InputStream)
            {
                stream.Seek(0, SeekOrigin.Begin);
                using (var reader = new StreamReader(stream))
                    bodyText = reader.ReadToEnd();
            }

            if (string.IsNullOrEmpty(bodyText)) return (null);

            var tweet = new JavaScriptSerializer().Deserialize<Models.test[]>(bodyText);

            return (tweet);
        }
    }
     * */
}