﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tp.ServiceReference1;
namespace tp.Models
{
  
    public class findObject
    {
     
        public List<string> users { get; set; }
        public List<string> schems { get; set; }
        public List<string> states { get; set; }
        public List<string> kategorii { get; set; }
        public List<shemsost> shem_sostoinii { get; set; }
        public filter param { get; set; }
    }
    public class zaivka_ajax
    {
        public zaivka_ajax(zaivka z, string type)
        {
            this.z = z;
            this.type = type;
        }
        public zaivka z { set; get; }
        public string type { set; get; }
    }
}