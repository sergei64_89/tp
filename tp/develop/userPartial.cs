﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Integro.InMeta.Runtime;
using tp.ServiceReference1;
namespace tp
{

    public static class HelpDevelopInmetaClass
    {
        public static InMeta.Session isogd
        {
            get
            {
                DataApplication app = new DataApplication("GradVisionIsogd");

                return new InMeta.Session(app, WindowsPrincipal.Current.Identity.Name);
            }
        }
        public static InMeta.General.Executor GetCurrentExecutor()
        {
            InMeta.Session session = isogd;
            InMeta.General.Executor ex = null;
            foreach (InMeta.General.Executor ext in session.General_Executor.Query(WindowsPrincipal.Current.Identity.Name))
            {
                try
                {
                    if (ext.Employee.Actor.Account.ToLower() == WindowsPrincipal.Current.Identity.Name.ToLower())
                    {
                        ex = ext;
                        break;
                    }
                }
                catch
                {
                }
            }
            return ex;
        }
    }

    public interface procedure
    {
        void setproc(ViewDataDictionary ViewData, zaivka z);
    }
    public static class userPartial
    {
        public static void get_parameters(zaivka z,string controller,ViewDataDictionary ViewData,TempDataDictionary TempData, out object parameters)
        {
            parameters = new object();
            switch (z.schema)
            {
                case "Подготовка разрешения на строительство":

                    Ппрзconfigur t = new Ппрзconfigur();
                    t.setproc(ViewData,z);
                    break;
                case "test_schem2":
                    ViewData["_Partialdetail"] = "_second";
                    break;
                case "test1":
                    ViewData["_Partialdetail"] = z.schema + "/Views/_s4";
                    break;
            }
        }
 
    }
}