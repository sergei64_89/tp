﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tp.special;
using tp.ServiceReference1;
using System.Data;
using System.Data.SqlClient;
using Integro.InMeta.Runtime;
using System.Xml.Serialization;
using System.IO;
using InMeta;
using Integro.InMeta.Runtime;
using System.Security.Principal;
using InMeta.Grad;
using InMeta.General;
using InMeta.Perm;

namespace tp.Controllers
{
    public partial class ТехнологическийпроцессподготовкиразрешениянастроительствоController : Controller
    {
      
        public  DataSession sess
        {
            get
            {
                DataApplication app = new DataApplication("GradVisionIsogd");
                DataSession sess = app.CreateSession();
                InMeta.Session current =new InMeta.Session(app,"timm");
              InMeta.General.Letter let=  current.General_Letter.AddNew();
              string ssssss = let.Id.ToString();
              current.Commit();
                return sess;
            }
        }
        private string getValue(string classvalue, string id, string propertyname)
        {
            DataId jj = new DataId(id);
            return sess[classvalue].GetObject(jj)[propertyname].ToString();
        }
        //
        public JsonResult setValue(string key, string txt)
        {
            DataSession ff = sess;

            Int64 test = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);
            Dictionary<string, string> list = new Dictionary<string, string>();
            zaivka z;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            client.get_zaivka_for_id(Auth.get_user(), test, out z);
            string h1, h2;
            if (txt == "")
            {
                z.fields.remove(key);
            }
            else
            {
                vspomogatelnii.parsestring(txt, out h1, out h2);
                if (h2 != null)
                {

                    if (z.fields[key] != null)
                    {
                        if (txt != "")
                        {
                            z.fields[key].value = h1;
                            z.fields[key].type = h2;
                        }
                        else
                            z.fields.remove(key);
                       
                    }
                    else
                    {

                        z.fields.Add(new fielding { key = key, type = h2, value = h1 });
                        
                    }

                }
            }
            z.Save();

            client.Close();
            return new JsonResult();
        }
        // GET: /process/
        //сохранить в переменную в заявку(инмета)
        #region
        private List<string> deserealize(string txt)
        {
            XmlSerializer ser = new XmlSerializer(typeof(List<string>));
            try
            {
                return (List<string>)ser.Deserialize(new StringReader(txt));
            }
            catch
            {
                return null;
            }
        }
        private string serealize(List<string> list)
        {
            XmlSerializer ser = new XmlSerializer(typeof(List<string>));
            System.Text.StringBuilder st=new System.Text.StringBuilder();
            StringWriter writer=new StringWriter(st);
            try
            {
                ser.Serialize(writer, list);
              return  st.ToString();
            }
            catch
            {
                return null;
            }
        }
        #endregion
        public ActionResult Добавить_еще_одного_заявителя()
        {
            Int64 number = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);
            zaivka z;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
          client.get_full_zaivka_for_id(Auth.get_user(), number, out z);
           
            InMeta.Session session =HelpDevelopInmetaClass.isogd;
            DataObject Заявитель = null;
            if (z.fields["заявитель"] != null)
            {
               
                if (z.fields["заявитель"].type == "Subject/Person")
                    Заявитель = session.Subject_Person.GetObject(new Integro.InMeta.Runtime.DataId(z.fields["заявитель"].value));
                if (z.fields["заявитель"].type == "Subject/Org")
                    Заявитель = session.Subject_Org.GetObject(new Integro.InMeta.Runtime.DataId(z.fields["заявитель"].value));
            }
            // Аггрегируем заявителя
            if (DataObject.Assigned(Заявитель))
            {
                InMeta.Subject.Org Организация = null;
                InMeta.Subject.Person ФизЛицо = null;
                if (Заявитель.Class.Name == "Subject/Person")
                {
                    ФизЛицо = (InMeta.Subject.Person)Заявитель;
                    if (ФизЛицо.FIORodProperty.IsNull)
                        TempData["error"] = "Заполните поле ФИО родительного падежа для данного физического лица";

                    if (ФизЛицо.FIODatProperty.IsNull)
                        TempData["error"] = "Заполните поле ФИО дательного падежа для данного физического лица";
                       
                }
                if (Заявитель.Class.Name == "Subject/Org")
                {
                    Организация = (InMeta.Subject.Org)Заявитель;
                    if (Организация.NameProperty.ValueDef() == "")
                        TempData["error"] = "Заполните поле наименование организации.";
                    if (Организация.NameRodProperty.ValueDef() == "")
                        TempData["error"] = "Заполните поле наименование(родительный падеж) организации.";
                    if (Организация.NameDatProperty.ValueDef() == "")
                        TempData["error"] = "Заполните поле наименование(дательный падеж) организации.";
                }
               
                if (TempData["error"] == null)
                {
                    InMeta.General.Letter Заявление = (InMeta.General.Letter)session.General_Letter.GetObject(new DataId(z.fields["заявление"].value));
                    InMeta.General.DocSubject Субъект = Заявление.General_DocSubject.AddNew();
                    Субъект.Subject = Заявитель;
                    session.Commit();

                    z.fields["count___"].value = (Convert.ToInt32(z.fields["count___"].value) + 1).ToString();
                    z.fields.remove("заявитель");
                    z.Save();
                }
            }
            return RedirectToAction("detail", "Detail", new { number = number });
        }
       
        public ActionResult Всезамечанияуказаны()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
                client.get_zaivka_for_id(Auth.get_user(),z,out za);

                InMeta.Session session = HelpDevelopInmetaClass.isogd;
                InMeta.Grad.ObjectRemark замечание=null;
            if (za.fields["замечание"]!=null)
                замечание = (InMeta.Grad.ObjectRemark)session.Grad_ObjectRemark.GetObject(new DataId(za.fields["замечание"].value));
                if (!DataObject.Assigned(замечание))
                {
                    TempData["error"] = String.Format("<br><br><font color=red>Не указаны замечания</font><br><br>");
                    return RedirectToAction("detail", "Detail", new { number = z });
                }
                




                za.fields.Add(new fielding { key = "f1", type = "", value = "true" });
                za.Save();
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");

            return RedirectToAction("detail", "Detail", new { number = z });
        }
        public ActionResult Регистрациязаявителя()
        {
            Int64 number = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
        

            #region
            zaivka z;

            client.get_full_zaivka_for_id(Auth.get_user(), number, out z);
            if (z==null)
                return View("userError");

            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            DataObject Заявитель = null;
            if (z.fields["заявитель"] != null)
            {

                if (z.fields["заявитель"].type == "Subject/Person")
                    Заявитель = session.Subject_Person.GetObject(new Integro.InMeta.Runtime.DataId(z.fields["заявитель"].value));
                if (z.fields["заявитель"].type == "Subject/Org")
                    Заявитель = session.Subject_Org.GetObject(new Integro.InMeta.Runtime.DataId(z.fields["заявитель"].value));
            }
            // Аггрегируем заявителя
            if (DataObject.Assigned(Заявитель))
            {
                InMeta.Subject.Org Организация = null;
                InMeta.Subject.Person ФизЛицо = null;
                if (Заявитель.Class.Name == "Subject/Person")
                {
                    ФизЛицо = (InMeta.Subject.Person)Заявитель;
                    if (ФизЛицо.FIORodProperty.IsNull)
                        TempData["error"] = "Заполните поле ФИО родительного падежа для данного физического лица";

                    if (ФизЛицо.FIODatProperty.IsNull)
                        TempData["error"] = "Заполните поле ФИО дательного падежа для данного физического лица";

                }
                if (Заявитель.Class.Name == "Subject/Org")
                {
                    Организация = (InMeta.Subject.Org)Заявитель;
                    if (Организация.NameProperty.ValueDef() == "")
                        TempData["error"] = "Заполните поле наименование организации.";
                    if (Организация.NameRodProperty.ValueDef() == "")
                        TempData["error"] = "Заполните поле наименование(родительный падеж) организации.";
                    if (Организация.NameDatProperty.ValueDef() == "")
                        TempData["error"] = "Заполните поле наименование(дательный падеж) организации.";
                }
                if (TempData["error"] == null)
                {
                    InMeta.General.Letter Заявление = (InMeta.General.Letter)session.General_Letter.GetObject(new DataId(z.fields["заявление"].value));
                    InMeta.General.DocSubject Субъект = Заявление.General_DocSubject.AddNew();
                    Субъект.Subject = Заявитель;
                    session.Commit();

                    z.fields["count___"].value = (Convert.ToInt32(z.fields["count___"].value) + 1).ToString();
                    z.fields.remove("заявитель");
                    z.Save();
                   
                }
            }
          
            #endregion
            if (Convert.ToInt32(z.fields["count___"].value)>=1)
            {
            error err = client.go_next_state(number, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            }
         



            return RedirectToAction("detail", "Detail", new { number = number });
        }
        public ActionResult Регистрациязаявлениявсистеме(FormCollection collection)
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);
            DateTime time1, time2;
            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_full_zaivka_for_id(Auth.get_user(), z, out za);

            try
            {
                string ghj = collection["dataregistration"];
                time1 = Convert.ToDateTime(collection["dataregistration"]);
                time2 = Convert.ToDateTime(collection["datasend"]);
            }
            catch
            {
                return RedirectToAction("detail", "Detail", new { number = z });
            }


            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.General.Letter Заявление = (InMeta.General.Letter)session.General_Letter.GetObject(new DataId(za.fields["заявление"].value));
            Заявление.OutLetterNo = collection["outputnumber"];
            Заявление.InLetterNo = collection["inputnumber"];
           
            Заявление.LetterRegDate = time1;
            Заявление.LetterSendDate = time2;

            InMeta.General.ExecutionDate ДатаИсполнения = Заявление.General_ExecutionDate.AddNew();
            ДатаИсполнения.Date = Заявление.LetterRegDate.AddDays(10);
            ДатаИсполнения.Description = "Дата исполнения (" + ДатаИсполнения.DateProperty.Value.ToShortDateString() + ") установленна автоматически из технологического процесса ID = " + za.id.ToString();
            ДатаИсполнения.Timing = 1;
            session.Commit();
          error err = client.go_next_state(z, Auth.get_user());
              ViewData["error_text"]=err.error_text;
           if (err.Is_error == true)
               return View("userError");

           return RedirectToAction("detail", "Detail", new { number = z });
        }
        public ActionResult CканированиеиразмещениеприлагаемыхкЗавлениюдокументов()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        public ActionResult ДобавитьЗамечание(FormCollection collection)
        {
            Int64 number = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);
            zaivka z;
            Auth.get_client().get_full_zaivka_for_id(Auth.get_user(), number, out z);

            InMeta.Session session = HelpDevelopInmetaClass.isogd;
           InMeta.Grad.ObjectRemark Замечание;
            string   ЗамечаниеПоЗаявлению=collection["zamechanie"];
            string Примечание = collection["primechanie"];
                    if (String.Empty.Equals(ЗамечаниеПоЗаявлению))
                       TempData["error"]= "<br><br><font color=red>Не указано содержание замечания к заявлению</font><br><br>";
                    InMeta.General.Letter Заявление = (InMeta.General.Letter)session.General_Letter.GetObject(new DataId(z.fields["заявление"].value));
                    Замечание = Заявление.Grad_ObjectRemark.AddNew();
                    Замечание.Remark = ЗамечаниеПоЗаявлению;
                    Замечание.Notes = Примечание;
                    Замечание.InObjectNo = Заявление.Grad_ObjectRemark.Count;
                    Замечание.RegDate = DateTime.Now;
                    //Замечание.Author = SurveyUtils.CurrentExecutor(Session);
                    session.Commit();
                    z.fields.Add(new fielding {key="замечание",type=Замечание.Class.Name,value=Замечание.Id.ToString() });
                    z.Save();    


            return RedirectToAction("detail", "Detail", new { number = number });
        }
        public ActionResult ДалееПерейтикопределениюкомплектадокументов(FormCollection collection)
        {
            string jjkkj=collection["Вид_строительства_val"];
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_full_zaivka_for_id(Auth.get_user(),z,out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.Perm.ConstrPermRequestProcess ПодготовкаРазрешения =( InMeta.Perm.ConstrPermRequestProcess) session.Perm_ConstrPermRequestProcess.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["ПодготовкаРазрешения"].value));



            ПодготовкаРазрешения.ObjectName = collection["nameobject"];
            ПодготовкаРазрешения.AddressObjectName = collection["address"];
               ПодготовкаРазрешения.BuildingKind = (InMeta.Grad.BuildingKind)session.Grad_BuildingKind.GetObject(new Integro.InMeta.Runtime.DataId(collection["Вид_строительства_val"]));
               ПодготовкаРазрешения.WorkKind = (InMeta.Grad.WorkKind)session.Grad_WorkKind.GetObject(new Integro.InMeta.Runtime.DataId(collection["Вид_работ_val"]));
           
            ПодготовкаРазрешения.RemoveFlag=collection["delete"]== "on" ? true :false;
            ПодготовкаРазрешения.ExpertizaFlag = collection["expert"] == "on" ? true : false; ;

            ПодготовкаРазрешения.CadNo = collection["zemli"];


            





            InMeta.Grad.GPZU ГПЗУ_ = (InMeta.Grad.GPZU)session.Grad_GPZU.GetObject(new Integro.InMeta.Runtime.DataId(collection["ГПЗУ_val"]));

            InMeta.General.SubRgn РайонГорода = (InMeta.General.SubRgn)session.General_SubRgn.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["РайонГорода_"].value));

            ПодготовкаРазрешения.GradPlan = ГПЗУ_;


            #region проверка на ошибки
            
            if (!Integro.InMeta.Runtime.DataObject.Assigned(ПодготовкаРазрешения.BuildingKind))
                TempData["error"] = "<font color=red>Не указан вид строительства</font>";
            if (!Integro.InMeta.Runtime.DataObject.Assigned(ПодготовкаРазрешения.WorkKind))
                TempData["error"] = "<font color=red>Не указан вид работ</font>";
            if (ПодготовкаРазрешения.NeedString("ObjectName") == "")
                TempData["error"] = "<font color=red>Не указано наименование объекта</font>";
            if (ПодготовкаРазрешения.NeedString("CadNo") == "")
                TempData["error"] = "<font color=red>Не указан кадастровый номер земельного участка</font>";
            if (!Integro.InMeta.Runtime.DataObject.Assigned(ПодготовкаРазрешения.GradPlan))
                TempData["error"] = "<font color=red>Не указан градостроительный план</font>";
            if (TempData["error"] != null)
                return RedirectToAction("detail", "Detail", new { number = z });
            #endregion



            if (ПодготовкаРазрешения.GradPlan.RegNoProperty.ValueDef() != "")
                ПодготовкаРазрешения.GPZU = ПодготовкаРазрешения.GradPlan.RegNo;
            if (DataObject.Assigned(РайонГорода))
                ПодготовкаРазрешения.SubRgn = РайонГорода;
      InMeta.Perm.ConstrPermBegunok      КомплектДокументов = ПодготовкаРазрешения.Perm_ConstrPermBegunok.AddNew();

      КомплектДокументов.RegNo = session.GenerateCustomId("CPBReq");
      КомплектДокументов.ComplectElem0 = true;
      КомплектДокументов.ComplectElem1 = true;
      КомплектДокументов.ComplectElem2 = true;
      КомплектДокументов.ComplectElem3 = true;
      КомплектДокументов.ComplectElem4 = true;
      КомплектДокументов.ComplectElem5 = true;
      КомплектДокументов.ComplectElem6 = true;
      КомплектДокументов.ComplectElem7 = true;
      КомплектДокументов.ComplectElem8 = true;
      КомплектДокументов.ComplectElem9 = true;
      КомплектДокументов.ComplectElem10 = true;
      КомплектДокументов.ComplectElem11 = true;
      КомплектДокументов.ComplectElem12 = true;
      КомплектДокументов.ComplectElem13 = true;
      КомплектДокументов.Complete = false;
      za.fields.Add(new fielding { key = "КомплектДокументов", type = КомплектДокументов.Class.Name, value = КомплектДокументов.Id.ToString() });
      za.fields.Add(new fielding { key = "ОтображатьКомплектДокументов", type = "", value = "true" });

      int opt1 = ПодготовкаРазрешения.BuildingKind.NeedInteger("Code");
   
      
      int opt4 = 1;
      if (ПодготовкаРазрешения.ExpertizaFlag == true)
          opt4 = 2;
      // Лин. объект не может быть без гос. экспертизы
      if (opt4 == 1 && opt1 == 3)
      {
          TempData["error"] = "Линейный объект не может быть без государственной экспертизы";
      return RedirectToAction("detail", "Detail", new { number = z });
      }

            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
           
            session.Commit();
            za.Save();
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        public ActionResult Заявлениеподготовленоправильноивсенеобходимыематериалыприложены()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);

            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            
            InMeta.Grad.ObjectRemark замечание = null;
            if (za.fields["замечание"] != null)
                замечание = (InMeta.Grad.ObjectRemark)session.Grad_ObjectRemark.GetObject(new DataId(za.fields["замечание"].value));
            if (DataObject.Assigned(замечание))
            {
                TempData["error"] = String.Format("<br><br><font color=red>Указаны замечания - продолжение невозможно</font><br><br>");
                return RedirectToAction("detail", "Detail", new { number = z });
            }
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");

            return RedirectToAction("detail", "Detail", new { number = z });

        }
        public ActionResult Удалитьвсезамечания()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.Grad.ObjectRemark Замечание = (InMeta.Grad.ObjectRemark)session.Grad_ObjectRemark.GetObject(new DataId(za.fields["замечание"].value));
            InMeta.General.Letter Заявление = (InMeta.General.Letter)session.General_Letter.GetObject(new DataId(za.fields["заявление"].value));
            if (DataObject.Assigned(Замечание))
            {
                Заявление.Grad_ObjectRemark.DeleteAll();
                
                session.Commit();
                za.fields.remove("замечание");
                za.Save();
            }
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        public ActionResult Перейтиканализукомплектностидокументов()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);

            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.Perm.ConstrPermBegunok КомплектДокументов = (InMeta.Perm.ConstrPermBegunok)session.Perm_ConstrPermBegunok.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["КомплектДокументов"].value));
            //Анализ бегунка
            if (КомплектДокументов.Complete == false)
            {
                TempData["error"] = ("Заполнение бегунка не закончено.");
                return RedirectToAction("detail", "Detail", new { number = z });
            }
          za.fields["ОтображатьКомплектДокументов"].value= "false";
          error err = client.go_next_state(z, Auth.get_user());
          ViewData["error_text"] = err.error_text;
          if (err.Is_error == true)
              return View("userError");

          return RedirectToAction("detail", "Detail", new { number = z });
            
        }
        public ActionResult Перейтиканализувозможностивыдачиразрешения()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
                InMeta.Perm.ConstrPermBegunok КомплектДокументов = (InMeta.Perm.ConstrPermBegunok)session.Perm_ConstrPermBegunok.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["КомплектДокументов"].value));
               
        bool h=    za.fields["КомплектностьДокументовПолная"].value == "true" ? true : false;


            if (!h)
            {
               TempData["error"]= "Подготовка разрешения невозможна. Не хватает следующих документов" + Ппрзconfigur.Проверить_бегунок(КомплектДокументов);
           return RedirectToAction("detail", "Detail", new { number = z });
            }
            else
            {
                  error err = client.go_next_state(z, Auth.get_user());
          ViewData["error_text"] = err.error_text;
          if (err.Is_error == true)
              return View("userError");

          return RedirectToAction("detail", "Detail", new { number = z });
             
            }

        }
        public ActionResult Перейтикформированиюотказа()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            za.fields.Add(new fielding { key = "OkFlag", type = "", value = "false" });
            za.fields.Add(new fielding { key = "f2", type = "", value = "true" });
            za.Save();
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");

            return RedirectToAction("detail", "Detail", new { number = z });
        }
        public ActionResult Ответсзамечаниямиподготовлен()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.General.OutLetter УведомлениеОтказ =(InMeta.General.OutLetter) session.General_OutLetter.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["УведомлениеОтказ"].value));
            DocStateHistory State = УведомлениеОтказ.Grad_DocStateHistory.AddNew();
            State.State = 1;
            State.StateDate = DateTime.Now.Date;


            InMeta.General.Executor ex = HelpDevelopInmetaClass.GetCurrentExecutor();
            State.Executor =ex ;
            session.Commit();

            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");

            return RedirectToAction("detail", "Detail", new { number = z });

        }

        
        private DocStateHistory GetNewState(int state, InMeta.General.OutLetter i_DataObject)
        {
            DocStateHistory State = i_DataObject.Grad_DocStateHistory.AddNew();
            State.State = state;
            State.StateDate = DateTime.Now.Date;
            State.Executor = HelpDevelopInmetaClass.GetCurrentExecutor();
            return State;
        }
        public ActionResult Ответ_согласован()
        {
             Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.General.OutLetter УведомлениеОтказ = (InMeta.General.OutLetter)session.General_OutLetter.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["УведомлениеОтказ"].value));
            DocStateHistory стадия = null;
            if (za.fields["стадия"]!=null)
             стадия=(DocStateHistory) session.Grad_DocStateHistory.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["стадия"].value));
            if (DataObject.Assigned(стадия))
            {
                TempData["error"] = String.Format("<br><br><font color=red>Указаны замечания - согласование невозможно</font><br><br>");
                return RedirectToAction("detail", "Detail", new { number = z });
            }
            DocStateHistory State = УведомлениеОтказ.Grad_DocStateHistory.AddNew();
            State.State = 2;
            State.StateDate = DateTime.Now.Date;

            State.Executor = HelpDevelopInmetaClass.GetCurrentExecutor();
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
            return RedirectToAction("detail", "Detail", new { number = z });
          
        }
        public ActionResult Добавить_замечание(FormCollection collection)
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.General.OutLetter УведомлениеОтказ = (InMeta.General.OutLetter)session.General_OutLetter.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["УведомлениеОтказ"].value));
            DocStateHistory стадия = null;
            if (za.fields["стадия"] != null)
                стадия = (DocStateHistory)session.Grad_DocStateHistory.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["стадия"].value));
            if (!DataObject.Assigned(стадия))
            {
                DocStateHistory State = УведомлениеОтказ.Grad_DocStateHistory.AddNew();
                State.State = 3;
                State.StateDate = DateTime.Now.Date;
                State.Executor = HelpDevelopInmetaClass.GetCurrentExecutor();
                стадия = State ;
                za.fields.Add(new fielding { key = "стадия", type = стадия.Class.Name, value = стадия.Id.ToString() });
            }
            if (String.Empty.Equals(collection["СодержаниеЗамечания"]))
            {
                TempData["error"] = String.Format("<br><br><font color=red>Не указано содержание замечания</font><br><br>");
                return RedirectToAction("detail", "Detail", new { number = z });
            }
            ObjectRemark Замечание = стадия.Grad_ObjectRemark.AddNew();
            Замечание.Remark =collection["СодержаниеЗамечания"];
            Замечание.Notes =collection["Примечание"];
            Замечание.InObjectNo = стадия.Grad_ObjectRemark.Count;
            Замечание.RegDate = DateTime.Now;
            //Замечание.Author = SurveyUtils.CurrentExecutor(Session);
            session.Commit();
            za.Save();
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        public ActionResult Удалить_все_замечания()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
           
            InMeta.General.OutLetter УведомлениеОтказ = (InMeta.General.OutLetter)session.General_OutLetter.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["УведомлениеОтказ"].value));
            DocStateHistory стадия = null;
            if (za.fields["стадия"] != null)
                стадия = (DocStateHistory)session.Grad_DocStateHistory.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["стадия"].value));
            if (DataObject.Assigned(стадия))
            {
                стадия.Delete();
                session.Commit();
                za.fields.remove("стадия");
                za.Save();
            }
            return RedirectToAction("detail", "Detail", new { number = z });
        }


        #region _Подпись ответа с замечаниями
        public ActionResult Ответподписан()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.General.OutLetter УведомлениеОтказ = (InMeta.General.OutLetter)session.General_OutLetter.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["УведомлениеОтказ"].value));
            InMeta.General.Letter Заявление = (InMeta.General.Letter)session.General_Letter.GetObject(new DataId(za.fields["заявление"].value));
            InMeta.Perm.ConstrPermRequestProcess ПодготовкаРазрешения = (InMeta.Perm.ConstrPermRequestProcess)session.Perm_ConstrPermRequestProcess.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["ПодготовкаРазрешения"].value));



            DocStateHistory State = УведомлениеОтказ.Grad_DocStateHistory.AddNew();
            State.State = 5;
            State.StateDate = DateTime.Now.Date;
            if (УведомлениеОтказ.OutLetterNoProperty.IsNull)
            {

                int iNo = 0;

                int cNo = session.GenerateCustomId("OutLet" + DateTime.Now.Year.ToString()) - 1;
                iNo = iNo + cNo;

                string regNo = String.Empty;
               // DataObject obj = .DataObject as DataObject;?
                regNo = iNo.ToString() + "/" + DateTime.Now.Year.ToString();
               

                УведомлениеОтказ.OutLetterNo = regNo;
            }
            if (УведомлениеОтказ.LetterDateProperty.IsNull)
                УведомлениеОтказ.LetterDate = DateTime.Now.Date;
            ExecutionDate ДатаИсполнения = Заявление.General_ExecutionDate.First;
            ДатаИсполнения.Timing = 3;
            //ПодготовкаРазрешения.CheckingRequestCompletedFailure(this);?
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
            return RedirectToAction("detail", "Detail", new { number = z });
          
        }
       //Добавитьзамечание из визирования ответа с замечаниями




        //Удалитьвсезамечания из визирования ответа с замечаниями
        
        public ActionResult Присвоитьписьмуномеридату()
        {

            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.General.OutLetter УведомлениеОтказ = (InMeta.General.OutLetter)session.General_OutLetter.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["УведомлениеОтказ"].value));

            if (УведомлениеОтказ.OutLetterNoProperty.IsNull)
            {

                int iNo = 0;

                int cNo = session.GenerateCustomId("OutLet" + DateTime.Now.Year.ToString()) - 1;
                iNo = iNo + cNo;

                string regNo = String.Empty;
                // DataObject obj = .DataObject as DataObject;?
                regNo = iNo.ToString() + "/" + DateTime.Now.Year.ToString();


                УведомлениеОтказ.OutLetterNo = regNo;
            }
            if (УведомлениеОтказ.LetterDateProperty.IsNull)
                УведомлениеОтказ.LetterDate = DateTime.Now.Date;
            session.Commit();
            return RedirectToAction("detail", "Detail", new { number = z });
        }


        #endregion
        #region Выдача отказа

        public ActionResult ОтветисопутствующиематериалыпереданыЗаявителю()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.General.OutLetter УведомлениеОтказ = (InMeta.General.OutLetter)session.General_OutLetter.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["УведомлениеОтказ"].value));
            DocStateHistory State = УведомлениеОтказ.Grad_DocStateHistory.AddNew();
            State.State = 6;
            State.StateDate = DateTime.Now.Date;

            State.Executor = HelpDevelopInmetaClass.GetCurrentExecutor();
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
            return RedirectToAction("detail", "Detail", new { number = z });
          
        }
        public ActionResult Завершитьпроцесс()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
           
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        #endregion
        #region Анализ возможности выдачи разрешения
        public ActionResult Перейтикподготовкеразрешения()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
           
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");

            return RedirectToAction("detail", "Detail", new { number = z });
        }
        public ActionResult Перейтикформированиюотказа2()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            za.fields.Add(new fielding { value = "true", type = "", key = "f3" });
            za.Save();
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
        
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        #endregion
        #region Ввод сведений об объекте застройки
       
        public ActionResult Данные_об_объекте_застройки_заполнены()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            GradChange ГрадостроительноеИзменение = null;
            DataObject ОбъектЗастройки = null;
            if (za.fields["ОбъектЗастройки"]!=null)
                ОбъектЗастройки = session[za.fields["ОбъектЗастройки"].type].GetObject(new DataId(za.fields["ОбъектЗастройки"].value));
            GradChangeStage Стадия = null;
            if (za.fields["Стадия"] != null)
                Стадия = (GradChangeStage)session.Perm_GradChangeStage.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["Стадия"].value));
            InMeta.Perm.ConstrPermRequestProcess ПодготовкаРазрешения = (InMeta.Perm.ConstrPermRequestProcess)session.Perm_ConstrPermRequestProcess.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["ПодготовкаРазрешения"].value));

            InMeta.General.Letter Заявление = (InMeta.General.Letter)session.General_Letter.GetObject(new DataId(za.fields["заявление"].value));

        


           
            if (ОбъектЗастройки == null)
               TempData["ОбъектЗастройки"]= String.Format("<br><br><font color=red>Необходимо выбрать или создать объект застройки</font><br><br>");
            foreach (InMeta.Perm.GradChange ГрИзм in ОбъектЗастройки.GetChilds("Perm/GradChange"))
            {
                if (ГрИзм.GradChangeKind == 1 && ПодготовкаРазрешения.WorkKind.Code == "1")
                    ГрадостроительноеИзменение = ГрИзм;
                if (ГрИзм.GradChangeKind == 2 && ПодготовкаРазрешения.WorkKind.Code == "2")
                    ГрадостроительноеИзменение = ГрИзм;
                if (ГрИзм.GradChangeKind == 3 && ПодготовкаРазрешения.WorkKind.Code == "3")
                    ГрадостроительноеИзменение = ГрИзм;
            }

            if (ГрадостроительноеИзменение == null)
            {
                ГрадостроительноеИзменение = session.Perm_GradChange.AddNew();
                ГрадостроительноеИзменение.GradChangeKind = Convert.ToInt32(ПодготовкаРазрешения.WorkKind.Code);
                ГрадостроительноеИзменение.GradObject = ОбъектЗастройки;
                ГрадостроительноеИзменение.Name = ОбъектЗастройки.GetView("default");
            }
            za.fields.Add(new fielding { key = "ГрадостроительноеИзменение", value = ГрадостроительноеИзменение.Id.ToString(), type = ГрадостроительноеИзменение.Class.Name });
            //throw new Exception(Top.Заявление.DataObject.General_DocSubject.First.Subject.Class.Name);
            if (Заявление.General_DocSubject.First.Subject.Class.Name.ToString() == "Subject/Org")
            {
                za.fields.Add(new fielding { key = "f4", value = "true", type = "" });
                za.Save();
            }
            else
            {
                //throw new Exception("1");
                Стадия = session.Perm_GradChangeStage.AddNew();
                Стадия.GradChange = ГрадостроительноеИзменение;
                Стадия.StageNo = 0;
                Стадия.StageName = "";
                Стадия.StageTotal = 0;
                Стадия.QueueNo = 0;
                Стадия.QueueName = "";
                Стадия.QueueTotal = 0;
                Стадия.RunComplexNo = 0;
                Стадия.RunComplexName = "";
                Стадия.RunComplexTotal = 0;
                ProjectTEP ПроектныеХарактеристики;
               
                ПроектныеХарактеристики = session.Perm_ProjectTEP.AddNew();
                ПроектныеХарактеристики.GradChangeStage = Стадия;


                za.fields.Add(new fielding { value = Стадия.Id.ToString(), type = Стадия.Class.Name, key = "Стадия" });
                za.fields.Add(new fielding { value = ПроектныеХарактеристики.Id.ToString(), type = ПроектныеХарактеристики.Class.Name, key = "ПроектныеХарактеристики" });
                
               

            }
             error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
            za.Save();
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        #endregion
        #region Ввод сведений о проектных характеристиках объекта
        public ActionResult Данные_об_объекте_застройки_заполнены2()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
         
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        #endregion

        #region
        public ActionResult Перейти_к_формированию_проекта_разрешения(FormCollection collection)
        {
             Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;

             ConstrPerm РазрешениеНаСтроительство = (ConstrPerm)session.Perm_ConstrPerm.GetObject(new DataId(za.fields["РазрешениеНаСтроительство"].value));
             РазрешениеНаСтроительство.ShortObjectDescription = collection["Краткие_характеристики_объекта"];
            РазрешениеНаСтроительство.ShortStageDescription=collection["Краткие_характеристики_стадии"];
            РазрешениеНаСтроительство.EndDate = Convert.ToDateTime(collection["Дата_формирования_разрешения"]);
            РазрешениеНаСтроительство.Signer = (Signer)session.Grad_Signer.GetObject(new Integro.InMeta.Runtime.DataId(collection["За_подписью_val"]));

            int days = 0;

            decimal СрокДействияМесяцы = Convert.ToDecimal(collection["Срок_действия_разрешения_в_месяцах"]);
            int СрокДействияДни = Convert.ToInt32(collection["Срок_действия_разрешения_в_днях"]);
            za.fields.Add(new fielding { key = "СрокДействияМесяцы", value = СрокДействияМесяцы.ToString(), type = "" });
            za.fields.Add(new fielding { key = "СрокДействияДни", value = СрокДействияДни.ToString(), type = "" });
            days = СрокДействияДни;
            int months = 0;
            //округляем 
            СрокДействияМесяцы = Math.Round(СрокДействияМесяцы, 1);
            months = Convert.ToInt16(Math.Truncate(СрокДействияМесяцы));

            decimal monthsDecimal = 0;
            monthsDecimal = СрокДействияМесяцы - Math.Truncate(СрокДействияМесяцы);

            days = days + Convert.ToInt16(Math.Truncate(monthsDecimal * 30));

            DateTime ValidDateCount = DateTime.Now.Date;
            ValidDateCount = ValidDateCount.AddDays(СрокДействияДни);

            //расчет ValidDate;
            РазрешениеНаСтроительство.ValidDate = РазрешениеНаСтроительство.ValidDate.AddDays(days + 1);
            РазрешениеНаСтроительство.ValidDate = РазрешениеНаСтроительство.ValidDate.AddMonths(months);
            РазрешениеНаСтроительство.ValidDate = РазрешениеНаСтроительство.ValidDate.Date;
          
            if (СрокДействияМесяцы == 0 && СрокДействияДни == 0)
            {
                TempData["error"] = "Не задан срок действия разрешения";



            }
            if (РазрешениеНаСтроительство.SignerProperty.IsNull)
               TempData["error"] = "Не указано лицо подписывающее разрешение";


            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
            za.Save();
            return RedirectToAction("detail", "Detail", new { number = z });

        }
            
        

        #endregion
        #region Перейти к утверждению разрешения
        public ActionResult Перейти_к_утверждению_разрешения()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_full_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            InMeta.Perm.ConstrPermRequestProcess ПодготовкаРазрешения = (InMeta.Perm.ConstrPermRequestProcess)session.Perm_ConstrPermRequestProcess.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["ПодготовкаРазрешения"].value));

            DocStateHistory стадия;

            стадия = ПодготовкаРазрешения.Grad_DocStateHistory.AddNew();

            стадия.StateDate = DateTime.Now.Date;
            стадия.Executor = HelpDevelopInmetaClass.GetCurrentExecutor();
            стадия.State = 2;
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        #endregion
        #region Утверждение разрешения
        public ActionResult Перейти_к_проставлению_реквизитов_разрешения()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;

            ConstrPerm РазрешениеНаСтроительство = (ConstrPerm)session.Perm_ConstrPerm.GetObject(new DataId(za.fields["РазрешениеНаСтроительство"].value));
            InMeta.Perm.ConstrPermRequestProcess ПодготовкаРазрешения = (InMeta.Perm.ConstrPermRequestProcess)session.Perm_ConstrPermRequestProcess.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["ПодготовкаРазрешения"].value));

            int СрокДействияДни =Convert.ToInt32( za.fields["СрокДействияДни"].value);
            decimal СрокДействияМесяцы = Convert.ToDecimal(za.fields["СрокДействияМесяцы"].value);




            int days = 0;
            days = СрокДействияДни;

            int months = 0;
            //округляем 
            СрокДействияМесяцы = Math.Round(СрокДействияМесяцы, 1);
            months = Convert.ToInt16(Math.Truncate(СрокДействияМесяцы));

            decimal monthsDecimal = 0;
            monthsDecimal = СрокДействияМесяцы - Math.Truncate(СрокДействияМесяцы);

            days = days + Convert.ToInt16(Math.Truncate(monthsDecimal * 30));

            DateTime ValidDateCount = DateTime.Now.Date;
            ValidDateCount = ValidDateCount.AddDays(СрокДействияДни);

            //расчет ValidDate;
            РазрешениеНаСтроительство.ValidDate = DateTime.Now.Date;
            РазрешениеНаСтроительство.ValidDate = РазрешениеНаСтроительство.ValidDate.AddDays(days + 1);
            РазрешениеНаСтроительство.ValidDate = РазрешениеНаСтроительство.ValidDate.AddMonths(months);
            РазрешениеНаСтроительство.ValidDate = РазрешениеНаСтроительство.ValidDate.Date;

            РазрешениеНаСтроительство.EndDate = DateTime.Now.Date;
           
            
                 DocStateHistory стадия;

                 стадия = ПодготовкаРазрешения.Grad_DocStateHistory.AddNew();

            стадия.StateDate = DateTime.Now.Date;
            стадия.Executor =HelpDevelopInmetaClass.GetCurrentExecutor() ;
            стадия.State = 5;
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
            za.Save();
            return RedirectToAction("detail", "Detail", new { number = z });

           
        }

        #endregion
        #region Проставление реквизитов разрешения
        public ActionResult Перейти_к_выдаче_разрешения(FormCollection collection)
        {

            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            ConstrPerm РазрешениеНаСтроительство = (ConstrPerm)session.Perm_ConstrPerm.GetObject(new DataId(za.fields["РазрешениеНаСтроительство"].value));
            РазрешениеНаСтроительство.PermNo = collection["Номер_разрешения"];
            РазрешениеНаСтроительство.EndDate = Convert.ToDateTime(collection["Дата_разрешения"]);
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
            za.Save();
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        #endregion
        #region выдача разрешения
        private void выдачаразрешенияhelp(zaivka z,Session session)
        {

           
            InMeta.Perm.ConstrPermRequestProcess ПодготовкаРазрешения = (InMeta.Perm.ConstrPermRequestProcess)session.Perm_ConstrPermRequestProcess.GetObject(new Integro.InMeta.Runtime.DataId(z.fields["ПодготовкаРазрешения"].value));

          
           DocStateHistory стадия;
           стадия = ПодготовкаРазрешения.Grad_DocStateHistory.AddNew();

           стадия.StateDate = DateTime.Now.Date;
           стадия.Executor = HelpDevelopInmetaClass.GetCurrentExecutor();
           стадия.State = 15;
        }
        public ActionResult Завершить_технологический_процесс()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            выдачаразрешенияhelp(za, session);
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
           
            return RedirectToAction("detail", "Detail", new { number = z });
        }
      
        public ActionResult Подготовить_ответ_заявителю_о_результатах_процесса()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            InMeta.Session session = HelpDevelopInmetaClass.isogd;
            выдачаразрешенияhelp(za, session);
            za.fields.Add(new fielding { key = "f5", value = "true", type = "" });
            za.Save();
            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
          
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        #endregion
        #region Ввод_сведений_о_стадии_градостроительного_изменения
        public ActionResult Данные_о_стадии_градостроительного_изменения_заполнены(FormCollection collection)
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            Session session = HelpDevelopInmetaClass.isogd;

            GradChange ГрадостроительноеИзменение = (GradChange)session.Perm_GradChange.GetObject(new DataId(za.fields["ГрадостроительноеИзменение"].value));
            GradChangeStage Стадия = (GradChangeStage)session.Perm_GradChangeStage.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["Стадия"].value));
            Стадия.StageNo =Convert.ToInt32( collection["НомерЭтапа"]);
            Стадия.StageName = collection["НазваниеЭтапа"].ToString() ;
         Стадия.QueueNo=Convert.ToInt32(    collection["НомерОчереди"] );
            Стадия.QueueName=collection["НазваниеОчереди"].ToString() ;
             Стадия.RunComplexNo=Convert.ToInt32(collection["НомерПусковогоКомплекса"]);
             Стадия.RunComplexName = collection["НазваниеПусковогоКомплекса"].ToString();
             Стадия.StageTotal=Convert.ToInt32(collection["ВсегоЭтапов"]) ;
            Стадия.QueueTotal=Convert.ToInt32(collection["ВсегоОчередей"]) ;
             Стадия.RunComplexTotal=Convert.ToInt32(collection["ВсегоПусковыхКомплексов"]);
             Стадия.GradChange = ГрадостроительноеИзменение;
            error err = client.go_next_state(z, Auth.get_user());
            collection["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
            
            za.Save();
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        #endregion
        #region
        public ActionResult Перейти_к_выдаче_отказа(FormCollection collection)
        {


            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            Session session = HelpDevelopInmetaClass.isogd;
            ConstrPermRefuse Отказ = (ConstrPermRefuse)session.Perm_ConstrPermRefuse.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["Отказ"].value));
            InMeta.Perm.ConstrPermBegunok КомплектДокументов = (InMeta.Perm.ConstrPermBegunok)session.Perm_ConstrPermBegunok.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["КомплектДокументов"].value));
            InMeta.Perm.ConstrPermRequestProcess ПодготовкаРазрешения = (InMeta.Perm.ConstrPermRequestProcess)session.Perm_ConstrPermRequestProcess.GetObject(new Integro.InMeta.Runtime.DataId(za.fields["ПодготовкаРазрешения"].value));

            Отказ.FailReason0 = collection["Не_полный_комплект_документов_"] == "on" ? true : false ;


            Отказ.FailReason1 = collection["Не_соответствует_наименование_объекта_"] == "on" ? true : false;



            Отказ.FailReason2 = collection["Место_размещение_объекта_по_проекту_не_соответствует_месту_допустимого_размещения_согласно_ГПЗУ_"] == "on" ? true : false;


            Отказ.FailReason3 = collection["Количество_этажей_или_высота_превышает_предельно_допустимое_значение_"] == "on" ? true : false;


            Отказ.FailReason4 = collection["Превышен_предельный_процент_застройки_земельного_участка_"] == "on" ? true : false;


            Отказ.FailReason5 = collection["Отсутствие_у_департамента_полномочий_по_выдаче_разрешения_на_строительство_на_данном_земельном_участке_"] == "on" ? true : false;


            Отказ.Signer = (Signer)session.Grad_Signer.GetObject(new Integro.InMeta.Runtime.DataId(collection["За_подписью_val"]));

            if (Отказ.FailReason0 == false &&
                      Отказ.FailReason1 == false &&
                      Отказ.FailReason2 == false &&
                      Отказ.FailReason3 == false &&
                      Отказ.FailReason4 == false &&
                      Отказ.FailReason5 == false &&
                      КомплектДокументов.Complete == true)
            {
                TempData["error"] = "Выдача отказа невозможна, так как комплектность бегунка полная, и не указана ни одна из причин отказа";
                return RedirectToAction("detail", "Detail", new { number = z });
            }
            if (Отказ.SignerProperty.IsNull)
            {
                TempData["error"] = "Не указано лицо подписывающее отказ";
                return RedirectToAction("detail", "Detail", new { number = z });
            }
            //Session session = (Session)Session.DataSession;
            //Top.Заявление.SetDocState(11, session);

            //Next(new Согласование_отказа());

            error err = client.go_next_state(z, Auth.get_user());
            ViewData["error_text"] = err.error_text;
            if (err.Is_error == true)
                return View("userError");
            session.Commit();
            za.Save();
            return RedirectToAction("detail", "Detail", new { number = z });
        }
        #endregion
    }
}
