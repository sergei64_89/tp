﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tp.special;
using tp.ServiceReference1;
using System.Data;
using System.Data.SqlClient;
using Integro.InMeta.Runtime;
using System.Xml.Serialization;
using System.IO;
namespace tp.Controllers
{
    public partial class DetailController : Controller
    {
        public ActionResult s4()
        {
            Int64 z = Convert.ToInt64(HttpContext.Request.Cookies["id_zaivki"].Value);

            ServiceReference1.PublisherServiceClient client = Auth.get_client();
            zaivka za;
            client.get_zaivka_for_id(Auth.get_user(), z, out za);
            client.go_next_state(z, Auth.get_user());
            client.Close();
            return RedirectToAction("detail", new { number = z });
        }
    }
}