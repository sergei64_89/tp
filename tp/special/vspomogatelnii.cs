﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using tp.special;

using System.Globalization;
using tp.Controllers;
using System.Collections.Specialized;
using System.Web.Mvc.Html;
using tp.ServiceReference1;
using Integro.InMeta.Runtime;
using tp.Models;
using System.Security.Policy;
namespace tp.special
{
    public static class vspomogatelnii
    {


        public static void get(Int64 Count, int PageNum,int max,ViewDataDictionary ViewData)
        {
            Int64 count;
            count = Count / max;
            if (Count % max > 0)
                count++;

            if ((PageNum < 0) || (count < PageNum + 1))
                PageNum = 0;
            if (Count < max)
            {
                ViewData["next"] = -1;
                ViewData["preview"] = -1;
            }
            else
            {
                ViewData["next"] = PageNum + 1;
                ViewData["preview"] = PageNum - 1;
            }

            if (PageNum == 0)
            {
                ViewData["preview"] = -1;
            }

            if ((count == PageNum + 1))
            {
                ViewData["next"] = -1;

            }



            ViewData["PageNum"] = PageNum;










        }

        public static string ConvertToDatetime(Int64 minute)
        {
          string help=  (minute / (60 * 24)).ToString()+" д ;";
          help += ((minute % (60 * 24)) / 60).ToString() + " ч ;";
          help += ((minute % (60 * 24)) % 60).ToString() + "  мин ;";
          return help;
        }
        public static void parsestring(string source,out string id, out string classname)
        {
            int n = source.IndexOf(':');
            classname = source.Substring(n+1);
            id = source.Substring(0,n);
        }

        

        public static MvcHtmlString listuser(this HtmlHelper html,List<string> names,string selectedname, string name)
        {
            string ret = "<select style='' name='" + name + "' id='" + name + "' >";
                foreach (string h in names)
                {
                    if (h==selectedname)
                    ret+="<option  selected='selected'>"+h+"</option>";
                    else
   ret+="<option>"+h+"</option>";
                }
            ret+=" </select>";
            return new MvcHtmlString(ret);
        }

        public static MvcHtmlString list(this HtmlHelper html, Dictionary<string, string> dict,string current, string name)
        {

            string ret =  "<input style='display:none' name='"+name+"val' id='"+name+"val' value='"+dict.FirstOrDefault().Key+"' /> <select style='max-width:150px' name='" + name + "' id='" + name + "' onchange='commitchanges(this)'>";
            
            foreach (KeyValuePair<string,string> h in dict)
            {
                if (h.Key == current)
                {
                    ret += "<option selected='selected' id='" + h.Key + "' >" + h.Value + "</option >";
                }
                else
                ret += "<option id='" + h.Key + "' >" + h.Value + "</option >";
            }
            ret += " </select>";
           // html.RenderPartial("_list", null);
            return new MvcHtmlString(ret);


        }
        public static MvcHtmlString IngeoUl(this HtmlHelper html, Dictionary<string,string> AtFirst,string url,zaivka z)
        {
            url = "/GradVisionIsogd/inmeta/object_data_form.asp" ;
            string help ="<ul style='margin-bottom:0px'>" ;
            string help1;
            foreach (fielding ff in z.fields.list)
            {
                if (ff.type.IndexOf('/') != -1)
                {
                  help1=  AtFirst.Where(n=>n.Key==ff.key).FirstOrDefault().Value;
                  if (help1 != null)
                      help += " <li style='display: inline'>" + "<a style='padding-right: 15px;' href='" + url + "?class=" + ff.type + "&id=" + ff.value + "' > " + help1 + " </a> </li>";

                }
               
            }
            help += "</ul>";
           
        return new MvcHtmlString(help);
        }
        public static MvcHtmlString IsError(this HtmlHelper html, string error)
        {
            if ((error != null) &&(error!=""))
                return new MvcHtmlString("<br><br><font color=red>" + error + "</font><br><br>");
            else return new MvcHtmlString("");
        }
        public static MvcHtmlString FilterField(this HtmlHelper html, filter ff,string action,string way)
        {
            if (ff == null)
                ff = new filter();
           string ret="";
           ret += "<button id='animfind'>Показать/Скрыть</button> <div id='find'> <form action='" + way + "' method='post' >   <input type='hidden' name='action' value='" + action + "'/> <table";
           ret += " style=' border: 1px solid;  border-radius: 10px; margin-left: 3px;'/>";
            string helpcheck="<td> <input name='{0}' type='checkbox' {2} /> {1} </td>";
            string helpinput="<td><input name='{1}'  value='{0}'/> </td>";
            #region вывод по состоянию
            if (ff.sost_value != null)
            {
                ret += "<tr>";
                ret += string.Format(helpcheck, new object[] { "sost", "По состоянию", "checked" });
                ret += string.Format(helpinput, new object[] { ff.sost_value,"sost_value" });
                ret += "</tr>";
            }
            else
            {
                ret += "<tr>";
                ret += string.Format(helpcheck, new object[] { "sost", "По состоянию", "" });
                ret += string.Format(helpinput, new object[] { "","sost_value" });
                ret += "</tr>";
            }
            #endregion


            #region вывод по статусу
            if (ff.state_value != null)
            {
                ret += string.Format(helpcheck, new object[] { "state", "По статусу", "checked" });
                ret += string.Format(helpinput, new object[] { ff.state_value,"state_value" });
            }
            else
            {
                ret += string.Format(helpcheck, new object[] { "state", "По статусу", "" });
                ret += string.Format(helpinput, new object[] { "","state_value" });
            }
                #endregion

            #region вывод по схеме
            if (ff.schem_value != null)
            {
                ret += "<tr>";
                ret += string.Format(helpcheck, new object[] { "schem", "По схеме", "checked" });
                ret += string.Format(helpinput, new object[] { ff.schem_value,"schem_value" });
                ret += "</tr>";
            }
            else
            {
                ret += "<tr>";
                ret += string.Format(helpcheck, new object[] { "schem", "По схеме", "" });
                ret += string.Format(helpinput, new object[] { "","schem_value" });
                ret += "</tr>";
            }
            #endregion
            ret += "<tr> <td colspan='2' style='text-align: center'> <button type='submit'>Поиск</button> </td> </tr> </table> <br /> </form> </div>";
            return new MvcHtmlString(ret);
        }
        public static MvcHtmlString link(this HtmlHelper html, string value, string id,string controller, string action,HttpRequestBase query, int number)
        {
            string ret = "<a href='{0}' id='{1}'>{2}</a>";
            string old = "";// старые парамметры,куда добавится или изменится PageNum
            //восстанавливаем строку get запроса(сохраняя старые параметры)
            NameValueCollection help = new NameValueCollection(query.QueryString);
           var urlHelper = new UrlHelper(html.ViewContext.RequestContext);
           action = urlHelper.Action(action, controller);
            //if (help.AllKeys.Where(n => n.ToString() == "PageNum").Count() == 0)
            //    help.Add("PageNum", number.ToString());
           // else
                help.Set("PageNum", number.ToString());
           //загружаем первый парамметр а дальше просто добавляем &
            if (help.Count>0)
            old += help.GetKey(0) + "=" + help.Get(0);
            for (int n = 1; n < help.Count; n++)

                old += "&" + help.GetKey(n) + "=" + help[n];
                  ret = string.Format(ret, new object[] { "" + action + "?" + old, id, value });
            return new MvcHtmlString(ret);
        }
        /// <param name="value">значение {ид:класс}  </param>
        /// <param name="id">ид скрытого поля в штмл</param>
        ///  <param name="list_class">Список классов интегро</param>
        ///  <param name="visibleClass">Визуализировать ли список классов</param>
        ///  <param name="servername">Имя сервера интегро</param>
        ///  <param name="valueshow">что выводить в поле (значение выбранного элемента ингео)</param>
        public static MvcHtmlString ingeo(this HtmlHelper html, string value, string id, Dictionary<string, string> list_class, string visibleClass, string servername, string ajaxcallbackController,string ajaxcallbackAction,Dictionary<string,string> valueshow,List<string> actions=null)
        {
          
            string idh,clas;
            ViewDataDictionary dict=new ViewDataDictionary();
    
           // list.Add("Subject/Org","Организация");
           // list.Add("Subject/Person","Физическое лицо");
            if (value == null)
            {
                dict["value"] = "";
                dict["displayvalue"] = "";
            }
            else
            {
                
                dict["value"] =value;//123434:Subject/Org
      
                vspomogatelnii.parsestring(value, out idh, out clas);

                DataApplication app = new DataApplication(servername);
                DataSession sess = app.CreateSession();
                DataObject obj = sess[clas].GetObject(new DataId(idh));

                try
                {
                    dict["displayvalue"] = obj[valueshow[clas]].ToString();   //
                }
                catch
                {
                    dict["displayvalue"] = obj.SystemView.Substring(obj.SystemView.IndexOf('(')+1, obj.SystemView.Length - obj.SystemView.IndexOf('(')-2);
                }
              
            }
            dict["ajaxcallbackController"] = ajaxcallbackController;
            dict["ajaxcallbackAction"] = ajaxcallbackAction;
            dict["script"] = true;//вывести ли скрипт
            dict["ClassSelector"] = list_class;//список классов ингео
            dict["visibleClass"] = visibleClass;// виден ли список классов
            dict["idinput"] = id;//id скрытого поля ввода
            dict["servername"] = servername;
            dict["actions"] =actions==null ? null: new List<string> (actions);
            html.RenderPartial("_integro",dict);
            
          return  new MvcHtmlString("");
        }
        public static MvcHtmlString ingeoattachedfiles(this HtmlHelper html, string classname, string id, string servername)
        {
            string help;
            help = "<a href='/"+servername+"/inmeta/attachments_ui.asp?class="+classname+"&amp;id="+id+"' target='_blank'>Прикрепленные Файлы</a>";
            return new MvcHtmlString(help);
        }


       
    }
}