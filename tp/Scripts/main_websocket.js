﻿
function class_animation(id_object) {

    var c;
    this.run = 0;
    this.begin_anim = function begin_anim() {
        this.run = 1;
        if (c == null)
            c = setInterval(function () {
                var h = $(id_object).css('color');
                h = h.replace(/\ /g, "");
                if (h == "rgb(201,237,219)") {
               
                    $(id_object).css("color", "rgb(255,0,0)");
                }
                else
                    $(id_object).css("color", "rgb(201,237,219)");



            }, 1000)
    }
    this.end_anim = function end_animation() {
        if (c != null) {
            clearInterval(c);
            this.run = 0;
            $(id_object).css("color", "rgb(201, 237, 219)");
        }
    }


}


function get_sending_tp(type, element) {




    $('#sending_zaivki').attr('sorted', element);
    var h = $('#' + element);
    var asc_desc = h.attr('asc_desc');
    if (asc_desc == "asc")
        h.attr('asc_desc', 'desc');
    else
        h.attr('asc_desc', 'asc');
    $.ajax({type:"Post",
        url: urlsendtp+"?type=" +encodeURIComponent( type) + "&asc_desc=gfhdfgh" , success: function (result) {
            var help = $('#sending_zaivki tbody');
            $('#sending_zaivki tbody tr').remove();
            for (var h = 0; h < result.length; h++) {
                help.append('<tr><td>' + result[h].state_name + '</td><td>' + result[h].schema + '</td><td>' + result[h].current_status + '</td><td>' + result[h].current_login + '</td>' +
                    '<td> <a href="' + urldetail + '?number=' + result[h].id + '" >Подробно </a></td><td> <a href="' + urltake + '?number=' + result[h].id + '" >Принять </a></td> </tr>');
               
            }

        }
    })
};
function isvalidation() {
    $.ajax(
        {
            type: 'Post',
            url: urlisvalidation,
            success: function (result) {
                if (result == false)
                    location = urlhome;
            }
        }
        );
};
$(document).ready(function () {
 
    setInterval(isvalidation,100000);

    var animation1 = new class_animation('#news');
    var animation2 = new class_animation('#get_sending_zaivki');
    var animation3 = new class_animation('#notificationcount');
    //при нажатии на принятие заявок
    $('#get_sending_zaivki').click(function () {
       
        $('#sending_zaivki').show();
        $('#newstp').hide();
        $('#table').hide();
        hide_a();
        animation2.end_anim();
        $('#news').css("color", "rgb(201, 237, 219)");
        get_sending_tp("dsd", "sdd");


    })
  
    // при нажатии на новые заявки
    $('#news').click(function () {
        $('#newstp').show();
        $('#sending_zaivki').hide();
        $('#table').hide();
        hide_a();
        animation1.end_anim();
        $('#news').css("color", "rgb(201, 237, 219)");
    })
    $('#notificationcount').click(function () {
        
        animation3.end_anim();
        
    })
    
    //Раскомментить в случае использования чистого websocket
    //Make sure you access http://localhost/index.htm as this hacky code works out the location of ws.ashx
    /*
    var url = 'ws://localhost:41206/websocket.ashx';



    var ws = new WebSocket(url);

    ws.onopen = function () {

    };

    ws.onmessage = function (e) {


        var help = JSON.parse(e.data);
        var h = help.z;
     
           
     
        //определение какое сообщение пришло(новая заявка или изменилось состояние старой)
        switch (help.type) {
            case 'new':
                animation1.begin_anim();
                $('#newstp tbody').append(' <tr id=' + h.id + '> <td> <a href="/Detail/detail?number=' + h.id + '" >Подробно </a></td><td>' + h.current_state + '</td><td>' + h.schema + '</td><td>' + h.current_status + '</td><td>' + h.current_login + '</td></tr>');
                break;
            case 'old':
                var help = $('#table tr[id=' + h.id + '] td');
                help[1].innerText = h.current_state;
                help[2].innerText = h.schema;
                break
            case 'status':
                var help = $('#table tr[id=' + h.id + '] td');
                help[3].innerText = h.current_status;
                break
            case 'remove':
                $('#table tr[id=' + h.id + '] td').remove();
                break;
            case 'user':
                animation2.begin_anim();
                $('#sending_zaivki tbody').append('<tr><td>' + h.current_state + '</td><td>' + h.schema + '</td><td>' + h.current_status + '</td><td>' + h.current_login + '</td>' +
                    '<td> <a href="/Detail/detail?number=' + h.id + '" >Подробно </a></td><td> <a href="/Detail/take?number=' + h.id + '" >Подробно </a></td> </tr>');
                break;
        }
    };
    
    */
    //Create Hub on Air


    function update(jquery, h) {
        if (jquery.length != 0) {
            jquery[0].innerText = h.state_name;
            jquery[1].innerText = h.schema;
            jquery[2].innerText = h.current_status;
        }
    }
    var wcfevents = new events();
    wcfevents.addNew(function (h) {
        animation1.begin_anim();
        $('#newstp tbody').append(' <tr id=' + h.id + '> <td>' + h.state_name + '</td><td>' + h.schema + '</td><td>' + h.current_status + '</td><td>' + h.current_login + '</td><td> <a href="' + urldetail + '?number=' + h.id + '" >Подробно </a></td></tr>');
       
    });
    wcfevents.addold(function (h) {
        var help = $('#table tr[id=' + h.id + '] td');
        update(help, h);
        help = $('#newstp tr[id=' + h.id + '] td');
        update(help, h);
        // help[0].innerText = h.state_name;
        // help[1].innerText = h.schema;
    });
    wcfevents.addstatus(function (h) {
        var help = $('#table tr[id=' + h.id + '] td');
        update(help, h);
        help = $('#newstp tr[id=' + h.id + '] td');
        update(help, h);
        // help[2].innerText = h.current_status;
    });
    wcfevents.addremove(function (h) {
        $('#table tr[id=' + h.id + '] td').remove();
    });
    wcfevents.adduser(function (h) {
        animation2.begin_anim();
        $('#sending_zaivki tbody').append('<tr><td>' + h.state_name + '</td><td>' + h.schema + '</td><td>' + h.current_status + '</td><td>' + h.current_login + '</td>' +
            '<td> <a href="'+urltake+'?number=' + h.id + '" >Подробно </a></td><td> <a href="Detail/take?number=' + h.id + '" >Подробно </a></td> </tr>');
      
    });
    wcfevents.addchangeduser(function (h) {
        var help = $('#table tr[id=' + h.id + '] td');
        help[3].innerText = h.current_login;
    
    });
    //Client Side Method To Access From Server Side Method
    

    
       

    

});




function hide_a() {
    $('#next').hide();
    $('#preview').hide();
}




